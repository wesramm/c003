ANSYS Report

ANSYS Logo: AnsysReportLogo.png

Date:  2016/11/16 14:47:43

______________________________________________________________________

Contents
1. File Report
    Table 1  File Information for HD_S1_Rev2_001
2. Mesh Report
    Table 2  Mesh Information for HD_S1_Rev2_001
3. Physics Report
    Table 3  Domain Physics for HD_S1_Rev2_001
    Table 4  Boundary Physics for HD_S1_Rev2_001
4. User Data
______________________________________________________________________

1. File Report

Table 1.  File Information for HD_S1_Rev2_001
Case	HD_S1_Rev2_001
File Path	C:\Users\engs1478\Google Drive\OU Sandbox\Projects\C003\Customer Supplied Docs\TBlock\S1_CFX_Case\HD_S1_Rev2_001.res
File Date	09 November 2016
File Time	03:43:40 PM
File Type	CFX5
File Version	17.1


2. Mesh Report

Table 2.  Mesh Information for HD_S1_Rev2_001
Domain	Nodes	Elements
S1	1759446	1704272


3. Physics Report

Table 3.  Domain Physics for HD_S1_Rev2_001
Domain - S1	
Type	Fluid
Location	"row01_VANE01_S1_001_O_PS, row01_VANE01_S1_001_O_PS1, row01_VANE01_S1_002_O_SS, row01_VANE01_S1_002_O_SS1, row01_VANE01_S1_005_H_IN, row01_VANE01_S1_006_H_PASS, row01_VANE01_S1_007_G_PASS, row01_VANE01_S1_008_H_TE, row01_VANE01_S1_009_H_EXIT"
Materials	
MGT Gas	
     Fluid Definition	Material Library
     Morphology	Continuous Fluid
Settings	
Buoyancy Model	Non Buoyant
Domain Motion	Stationary
Reference Pressure	 0.0000e+00 [Pa]
Heat Transfer Model	Total Energy
     Include Viscous Work Term	True
Turbulence Model	SST
Turbulent Wall Functions	Automatic
     High Speed Model	Off
Domain Interface - S1 to S1 Periodic 1	
Boundary List1	S1 to S1 Periodic 1 Side 1
Boundary List2	S1 to S1 Periodic 1 Side 2
Interface Type	Fluid Fluid
Settings	
Interface Models	Rotational Periodicity
     Axis Definition	Coordinate Axis
     Rotation Axis	Coord 0.1
Mesh Connection	Automatic
Domain Interface - S1 to S1 Periodic 2	
Boundary List1	S1 to S1 Periodic 2 Side 1
Boundary List2	S1 to S1 Periodic 2 Side 2
Interface Type	Fluid Fluid
Settings	
Interface Models	Rotational Periodicity
     Axis Definition	Coordinate Axis
     Rotation Axis	Coord 0.1
Mesh Connection	Automatic
Domain Interface - S1 to S1 Periodic 3	
Boundary List1	S1 to S1 Periodic 3 Side 1
Boundary List2	S1 to S1 Periodic 3 Side 2
Interface Type	Fluid Fluid
Settings	
Interface Models	Rotational Periodicity
     Axis Definition	Coordinate Axis
     Rotation Axis	Coord 0.1
Mesh Connection	Automatic
Domain Interface - S1 to S1 Periodic 4	
Boundary List1	S1 to S1 Periodic 4 Side 1
Boundary List2	S1 to S1 Periodic 4 Side 2
Interface Type	Fluid Fluid
Settings	
Interface Models	Rotational Periodicity
     Axis Definition	Coordinate Axis
     Rotation Axis	Coord 0.1
Mesh Connection	Automatic


Table 4.  Boundary Physics for HD_S1_Rev2_001
Domain	Boundaries	
S1	Boundary - inlet	
	Type	INLET
	Location	row01Inlet
	Settings	
	Flow Direction	Cartesian Components
	     Unit Vector X Component	S1 Inlet.Velocity u(radius)
	     Unit Vector Y Component	S1 Inlet.Velocity v(radius)
	     Unit Vector Z Component	S1 Inlet.Velocity w(radius)
	Flow Regime	Subsonic
	Heat Transfer	Total Temperature
	     Total Temperature	S1 Inlet.Total Temperature(radius)
	Mass And Momentum	Total Pressure
	     Relative Pressure	S1 Inlet.Total Pressure(radius)
	Turbulence	Medium Intensity and Eddy Viscosity Ratio
	Boundary - S1 to S1 Periodic 1 Side 1	
	Type	INTERFACE
	Location	Primitive 2D CM
	Settings	
	Heat Transfer	Conservative Interface Flux
	Mass And Momentum	Conservative Interface Flux
	Turbulence	Conservative Interface Flux
	Boundary - S1 to S1 Periodic 1 Side 2	
	Type	INTERFACE
	Location	Primitive 2D CY
	Settings	
	Heat Transfer	Conservative Interface Flux
	Mass And Momentum	Conservative Interface Flux
	Turbulence	Conservative Interface Flux
	Boundary - S1 to S1 Periodic 2 Side 1	
	Type	INTERFACE
	Location	Primitive 2D FK
	Settings	
	Heat Transfer	Conservative Interface Flux
	Mass And Momentum	Conservative Interface Flux
	Turbulence	Conservative Interface Flux
	Boundary - S1 to S1 Periodic 2 Side 2	
	Type	INTERFACE
	Location	Primitive 2D FD
	Settings	
	Heat Transfer	Conservative Interface Flux
	Mass And Momentum	Conservative Interface Flux
	Turbulence	Conservative Interface Flux
	Boundary - S1 to S1 Periodic 3 Side 1	
	Type	INTERFACE
	Location	Primitive 2D FT
	Settings	
	Heat Transfer	Conservative Interface Flux
	Mass And Momentum	Conservative Interface Flux
	Turbulence	Conservative Interface Flux
	Boundary - S1 to S1 Periodic 3 Side 2	
	Type	INTERFACE
	Location	Primitive 2D FS
	Settings	
	Heat Transfer	Conservative Interface Flux
	Mass And Momentum	Conservative Interface Flux
	Turbulence	Conservative Interface Flux
	Boundary - S1 to S1 Periodic 4 Side 1	
	Type	INTERFACE
	Location	Primitive 2D FU
	Settings	
	Heat Transfer	Conservative Interface Flux
	Mass And Momentum	Conservative Interface Flux
	Turbulence	Conservative Interface Flux
	Boundary - S1 to S1 Periodic 4 Side 2	
	Type	INTERFACE
	Location	Primitive 2D FV
	Settings	
	Heat Transfer	Conservative Interface Flux
	Mass And Momentum	Conservative Interface Flux
	Turbulence	Conservative Interface Flux
	Boundary - outlet	
	Type	OUTLET
	Location	row01Outlet
	Settings	
	Flow Regime	Subsonic
	Mass And Momentum	Average Static Pressure
	     Pressure Profile Blend	 5.0000e-02
	     Relative Pressure	 1.0000e+06 [Pa]
	Pressure Averaging	Average Over Whole Outlet
	Boundary - S1 Blade	
	Type	WALL
	Location	"p6 3, p14, row01Blade_Wall_ps, row01Blade_Wall_ss, row01Blade_leak_ps1, row01Blade_leak_ps2, row01Blade_leak_ss"
	Settings	
	Heat Transfer	Adiabatic
	Mass And Momentum	No Slip Wall
	Wall Roughness	Smooth Wall
	Boundary - S1 Hub	
	Type	WALL
	Location	"row01Hub_Wall, row01Hub_leak_iasle, row01Hub_leak_iaste"
	Settings	
	Heat Transfer	Adiabatic
	Mass And Momentum	No Slip Wall
	Wall Roughness	Smooth Wall
	Boundary - S1 Tip	
	Type	WALL
	Location	"row01Tip_Wall, row01Tip_leak_oasle, row01Tip_leak_oaste"
	Settings	
	Heat Transfer	Adiabatic
	Mass And Momentum	No Slip Wall
	Wall Roughness	Smooth Wall


4. User Data


