# Sample .bashrc for SuSE Linux                                               
# Copyright (c) SuSE GmbH Nuernberg

# There are 3 different types of shells in bash: the login shell, normal shell
# and interactive shell. Login shells read ~/.profile and interactive shells
# read ~/.bashrc; in our setup, /etc/profile sources ~/.bashrc - thus all
# settings made here will also take effect in a login shell.
#
# NOTE: It is recommended to make language settings in ~/.profile rather than
# here, since multilingual X sessions would not work properly if LANG is over-
# ridden in every subshell.

# Some applications read the EDITOR variable to determine your favourite text
# editor. So uncomment the line below and enter the editor of your choice :-)
#export EDITOR=/usr/bin/vim
#export EDITOR=/usr/bin/mcedit

# For some news readers it makes sense to specify the NEWSSERVER variable here
#export NEWSSERVER=your.news.server

# If you want to use a Palm device with Linux, uncomment the two lines below.
# For some (older) Palm Pilots, you might need to set a lower baud rate
# e.g. 57600 or 38400; lowest is 9600 (very slow!)
#
#export PILOTPORT=/dev/pilot
#export PILOTRATE=115200

#test -s ~/.alias && . ~/.alias || true

PS1="\u@\h:\W > "

alias ls='/bin/ls --color'
alias ll='ls -l'
alias la='ll -a'
alias h=history
alias gnuplot="gnuplot -noraise"

export $(dbus-launch) 

alias paraview=/usr/local/ParaView-5.0.0/bin/paraview

test -s ~/.alias && . ~/.alias || true

shell=`/bin/basename \`/bin/ps -p $$ -ocomm=\``
if [ -f /usr/share/Modules/init/$shell ]
then
  . /usr/share/Modules/init/$shell
else
  . /usr/share/Modules/init/sh
fi

set bell-style none

alias  kate='kate -n'

module load use.own
module load fv_13
module load tecplot
module load pgplot_gfortran
module load tblock
module load ansys_171
module load numeca
module load fluent
module load meristro
module load gichtgas
module load nist


export  CFXLIC=1055@mtdeobhaslic02
#export  FLUENTLIC=7241@mtdeobhaslic02
export  FLUENTLIC=7241@dtdeobhas011

export  NAGLIC=7312@tts0010f
export  FVLIC=1602@mtdeobhaslic02

export  LM_LICENSE_FILE="$CFXLIC : $FLUENTLIC : $NAGLIC : $FVLIC"

export PATH=/home/hamdy/bin:$PATH

#export PATH=$PATH:/usr/lib64/openmpi/bin

